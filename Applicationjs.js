/*REFERENCES: https://www.w3schools.com/howto/howto_js_dropdown.asp
https://www.w3schools.com/howto/howto_js_accordion.asp */


/* When the button is clicked,
toggle between hiding and showing the dropdown menu content */
function DropdownFunction() {
  document.getElementById("MenuDropdown").classList.toggle("show");
}

/* Dropdown menu closes if clicked outside of it */
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}


var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  /* This toggles between the adding and removing of the class that is active to highlight that button which controls
     the panel*/
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    /* This is used to toggle between the showing and hiding of the panel that's active */
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}
